int midiNote;
int midiOctave;
[0,2,3,5,7,8,10,0,2,3,5,7,8,10,0,2] @=> int minorScale[];
[0,7,3,5,8,10,2,0,7,3,5,8,10,2,0,7] @=> int minorOrdered[]; // scale: 1st, 5th, 3rd, 4th, 6th, 7th, 2nd
[0,2,4,5,7,9,11,0,2,4,5,7,9,11,0,2] @=> int majorScale[];
[0,7,4,5,9,11,2,0,7,4,5,9,11,2,0,7] @=> int majorOrdered[]; // scale: 1st, 5th, 3rd, 4th, 6th, 7th, 2nd
["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"] @=> string noteNames[];
3 => int polyphony;
float filtFreq;
float filtReso;
50 => float filtGain;
0 => float mu;  // mean - center point
1 => float sigma;    // standard deviation
Math.gauss(0, mu, sigma) => float maxGaussValue;
87 => float tempo;
[1., 1.5, 2., 3.] @=> float syncRates[];
["4", "4T", "8", "8T"] @=> string syncRateNames[];
1 => float syncRate;
75 => float likelihood; // likelihood that "syncRate" changes
0.2 => float reverbMix;


Impulse impulse[polyphony];
ResonZ filter[polyphony];
Pan2 panpot[polyphony];

NRev reverb[2];
reverb[0] => dac.left;
reverb[1] => dac.right;

reverb[0].mix(reverbMix);
reverb[1].mix(reverbMix);


// initialize array of UGen
for(0 => int c; c<polyphony; c++)
{
    impulse[c] => filter[c] => panpot[c];
    panpot[c].left => reverb[0];
    panpot[c].right => reverb[1];
    filter[c].gain(filtGain);
}

//-------------MAIN--------------
while(true)
{
    if(Math.random2f(0,100) > likelihood)
    {
        Math.random2(0,syncRates.size()-1) => int i;
        syncRates[i] => syncRate;

        <<< "SYNC RATE: " + syncRateNames[(syncRate-1) $ int] >>>;
    }
    for(0 => int c; c<polyphony; c++)
    {
        (Math.random2(1,3) * 12) + 36 => midiOctave;    // select octave
        gaussScale(minorOrdered) => midiNote;  // pick a random (gaussian) note from the 'ordered array'
        Std.mtof(midiNote + midiOctave) => filtFreq;
        Math.random2f(30,200) => filtReso;
        filter[c].set(filtFreq, filtReso);
        Math.random2(0,1) => int playStop;
        Math.random2f(-0.8,0.8) => float voicePan;
        panpot[c].pan(voicePan);
        impulse[c].next(playStop);
        if(playStop)
            chout <= noteNames[midiNote] <= (midiNote+midiOctave)/12 <= " ";    // print note name if played
    }

    chout <= IO.nl();

    // bpm * 0.5 = 8th
    ((bpm2ms(tempo)) / syncRate)::ms => now;
}


//-----------------FUNCTIONS------------------
function int gaussScale(int a[])
{
    //  pick a random (gaussian) value from an array
    Math.random2f(0,1) => float x;
    Math.gauss(x, mu, sigma) / maxGaussValue => float gauss;
    a.size() - Math.round(gauss*a.size()) => gauss;
    return a[gauss $ int];
}

function float bpm2ms(float x)
{
    return (60000./x);
}
