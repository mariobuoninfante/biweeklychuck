DROPS - Mario Buoninfante 29th OCT 2018
---

Drops randomly generates "harmony and melody". The script decides the length of the "composition", the initial scale (major/minor), when and where to "modulate".
The idea was to experiment with different voices, all controlled by the same phasor. The random processes are simple, no complex rules have been used to enhance the tonal aspect of the composition.
The script uses the FaucK chugin. It is strongly raccommended to run it from the terminal (FaucK doesn't seem to work fine with miniAudicle)