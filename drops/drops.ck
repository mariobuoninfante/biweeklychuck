//-----------------------------------------------
//--------------------CLASSES--------------------
//-----------------------------------------------
class Drop extends Chubgraph
{
    inlet => Faust wrap => ResonZ filter => outlet;
    Sitar sitar => Gain sitarGain => ResonZ sitarFilter => outlet;

    wrap.compile("Wrap.dsp");

    500 => float cutoff;
    15 => float Q;
    sitarGain.gain(0);

    function void playNote(float x, float y, float volume, dur tempo)
    {
        x => cutoff;
        y => Q;
        dur tempoAttack;

        sitar.noteOff(0.5);

        if(Math.random2(0,100) > 65)
        {
            Math.random2f(0.01,1) => float timeDiv;
            tempo*timeDiv => tempoAttack;
            tempoAttack => now;
        }
        else
        {
            0::second => tempoAttack;
        }

        filter.gain(volume);
        filter.set(cutoff, Q);
        sitarFilter.set(cutoff*3, Q*0.15);
        this.setSitarFreq(cutoff, tempo-tempoAttack);
    }

    function void setSitarFreq(float x, dur tempo)
    {
        0 => int amIPlaying;
        sitar.freq(x);
        Math.random2f(0,1) => float rPluck;
        Math.random2f(0.5,1) => float rVelOn;
        Math.random2f(0.25,1) => float rVelOff;
        Math.random2f(1,2) => float timeDiv;

        // decide whether Sitar should play or not
        if(Math.random2(0,100) > 80)
        {
            1 => amIPlaying;
            Math.random2f(0.9,2) => float sitarVol;
            sitarGain.gain(sitarVol);
        }
        else
        {
            0 => amIPlaying;
            sitarGain.gain(0);
        }

        if(amIPlaying == 1)
        {
            sitar.pluck(rPluck);
            sitar.noteOn(rVelOn);
            tempo*2 => now;

            //decide whether Sitar should stop the note or not
            if(Math.random2(0,100) > 65)
            {
                sitar.noteOff(rVelOff);
            }
        }
    }

    function void setWrap(float x, float y)
    {
        wrap.v("/Wrap/gain", x);
        wrap.v("/Wrap/mod", y);
    }

}
//------------------------------------------------;
//----------------CLASSES-END---------------------;
//------------------------------------------------;

[0,2,3,5,7,8,10] @=> int minorScale[];
[0,2,4,5,7,9,11] @=> int majorScale[];
[1,1,0,1,1,0,0] @=> int fromMinToMaj[]; //0: major, 1: minor
[0,1,1,0,0,1,1] @=> int fromMajToMin[]; //0: major, 1: minor
int currentScale[7];
int scaleMode;
string scaleModeName;
24 => int offset;   //lowest possible octave
0 => int transpOffset;
int modDest;    //when modulate, do it here
-1 => int modWhen;  //point in time at which modulation happens - based on loopCounter
-1 => int distWhen;
0 => int loopCounter;
-1 => int end;  //when sound stop - based on loopCounter
5 => int fadeTime;  //fade in/out time
7 => int distFadeIn;

//-----------------------------------------
//decide the mode and the structure
if(Math.random2(0,1) == 0)
{
    majorScale @=> currentScale;
    0 => scaleMode;
    "MAJOR SCALE" => scaleModeName;
}
else
{
    minorScale @=> currentScale;
    1 => scaleMode;
    "MINOR SCALE" => scaleModeName;
}

Math.random2(20,40) => modWhen;
Math.random2(1,6) => modDest;
Math.random2(15, 30) + modWhen => end;
Math.random2(10,end-10) => distWhen;
for(0=>int c; c<3; c++)
{
    <<< "----------------------------------------------------------" >>>;
}
<<< "SCALE: " + scaleModeName >>>;
<<< "AFTER: " + modWhen + " LOOPS" + " TRANSPOSE TO: " + (modDest+1) >>>;
/*<<< "DISTORTION KICKS IN AT: " + distWhen >>>;*/
<<< "END AT LOOP NR: " + end >>>;
for(0=>int c; c<3; c++)
{
    <<< "----------------------------------------------------------" >>>;
}

2::second => now;
//-----------------------------------------
//-----------------------------------------

4 => int polyphony;

Drop drop[polyphony];
Pan2 p[polyphony];
Pan2 aux[polyphony];
Phasor phasor;
GVerb reverb;
Gain aS;
Gain auxSend;
Gain outLeft;
Gain outRight;
Gain gL;    //drop objects are connected here
Gain gR;    //drop objects are connected here
Step step => Envelope env;  //interpolator used for fade in/out
//Step dStep => Envelope dist => dac;  //interpolator used as distortion (amplitude offset)
env => outLeft;
env => outRight;
//"aS" plays the same role as "outLeft" and "outRight"
aS => auxSend;
env => auxSend => reverb => dac;


step.next(1);
env.time(5);
auxSend.op(3);
outLeft.op(3);
outRight.op(3);

/*dStep.next(1);
dist.time(distFadeIn);*/


// GVerb OPTIONS:
// roomsize: (float) [1.0 - 300.0], default 30.0
// revtime: (dur), default 5::second
// damping: (float), [0.0 - 1.0], default 0.8
// spread: (float), default 15.0
// inputbandwidth: (float) [0.0 - 1.0], default 0.5
// dry (float) [0.0 - 1.0], default 0.6
// early (float) [0.0 - 1.0], default 0.4
// tail (float) [0.0 - 1.0], default 0.5

reverb.revtime(1.5::second);
reverb.roomsize(250);
reverb.early(0.1);
reverb.tail(0.45);
reverb.dry(0);
reverb.damping(0.3);

for(0=>int c; c<drop.size(); c++){
    phasor => drop[c] => p[c];
    p[c].gain(0.9);
    p[c].left => gL;
    p[c].right => gR;
    aux[c].gain(1);
    p[c] => aux[c] => aS;
}

reverb.gain(5);

0.5::second => now;

3 => float phasorFreq;
phasor.freq(phasorFreq);

//"drop" objects are not directly connected to "outLeft" and "outRight"
//in order to use these 2 gains in multiplier mode (.op(3)).
//Otherwise all the "drop" objects would be multiplied to each other
gL => outLeft => dac.left;
gR => outRight => dac.right;

//--------------------------------------------
//--------------------MAIN--------------------
//--------------------------------------------
while(true)
{
    //only at the beginning
    if(loopCounter == 0)
    {
        env.keyOn(1);
        <<< "FADE IN" >>>;
    }
    //when modulation happens
    else if(loopCounter == modWhen)
    {
        transposeScale(currentScale, modDest) => scaleModeName;
        <<< "-----------------------------" >>>;
        <<< "MODULATION TO: " + (modDest+1) + " " + scaleModeName + "--------" >>>;
        <<< "-----------------------------" >>>;
    }
    //only at the end
    else if(loopCounter == end)
    {
        env.keyOff(1);
        <<< "FADE OUT" >>>;
        (fadeTime+5)::second => now;

        <<< "PROGRAM ENDED" >>>;

        break;
    }

    if(Math.random2(0,100) > 70)
    {
        Math.random2f(1,4) => phasorFreq;
        phasor.freq(phasorFreq);
    }

    dropWrap(phasorFreq) => float t;
    dropFilter((t*0.5)::second, transpOffset, currentScale);
    dropPan();

    loopCounter++;
    <<< "------------------------LOOP: " + loopCounter >>>;
    t::second => now;
}


//-----------------------------------------------
//------------------FUNCTIONS--------------------
//-----------------------------------------------
function void dropFilter(dur t, int transpose, int scale[])
{
    int midiNote;
    1 => float scaleVol;

    for(0=>int c; c<drop.size(); c++){
        (Math.random2(1,4) * 12) + offset => float octaveOffset;    //change octave randomly
        (Math.random2(0,6) + transpose) % 7 => int x;
        Math.random2f(350,600) => float y;
        (scale[x]+octaveOffset) $ int => midiNote;

        //scale filter volume in accord with octave
        (1. / ((midiNote/12) - 2)) => scaleVol;
        (scaleVol*scaleVol) * 2.5 => scaleVol;

        spork ~ drop[c].playNote(Std.mtof(midiNote), y, scaleVol, t);
    }
}

function void dropPan()
{
    for(0=>int c; c<drop.size(); c++){
        Math.random2f(-1,1) => float panpot;
        p[c].pan(panpot);
    }

}

function float dropWrap(float fr)
{
    int r1;
    1 => int r2;

    for(0=>int c; c<drop.size(); c++){
        Math.random2(1, 4) => r1;   //pick a random subdivision (ie 1/1, 1/1T, 1/2, 1/2T, etc.)
        drop[c].setWrap(r1, 1);
        Math.max(r1,r2) $ int => r2;    //save the shortest division in order to return that
    }

    Math.random2(1,3) => int repetition;    //randomly select how many times before call this function again (dropWrap())
    (repetition * (fr/(r2 $ float))) => float t;

    return t;
}

function string transposeScale(int scale[], int offset)
{
    int transposeTo;
    string mode;

    if(scaleMode == 0)
    {
        //scale in use is MAJOR
        fromMajToMin[offset] => transposeTo;
    }
    else
    {
        //scale in use is MINOR
        fromMinToMaj[offset] => transposeTo;
    }

    if(transposeTo == 0)
    {
        //transpose to MAJOR
        majorScale @=> currentScale;
        "MAJOR SCALE" => mode;
    }
    else
    {
        //transpose to MINOR
        minorScale @=> currentScale;
        "MINOR SCALE" => mode;
    }

    offset => transpOffset; //apply transposition factor

    return mode;
}
